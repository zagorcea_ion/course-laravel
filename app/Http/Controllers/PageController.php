<?php

namespace App\Http\Controllers;



use App\Models\Blog;
use App\Models\BlogCategory;

class PageController extends Controller
{

    /**
     * Show home page
     *
     * @return view
     */
    public function home()
    {
        $news = Blog::with('category')
            ->orderBy('created_at', 'desc')
            ->limit(9)
            ->get();

        $recentNews = Blog::with('category')
            ->orderBy('created_at', 'desc')
            ->limit(4)
            ->get();

        return view('public.home-page', compact('recentNews', 'news'));
    }

    /**
     * Show news page
     *
     * @param int|null $category - filter by category id
     * @return view
     */
    public function news(int $category = null)
    {
        if (is_null($category)) {
            $news = Blog::with('category')
                ->orderBy('created_at', 'desc')
                ->paginate(5);

        } else {
            $news = Blog::where('id_category', $category)
                ->with('category')
                ->orderBy('created_at', 'desc')
                ->paginate(5);
        }

        $categories = BlogCategory::orderBy('created_at', 'desc')
            ->limit(5)
            ->get();

        $topNews = Blog::orderByRaw('RAND()')
            ->limit(4)
            ->get();

        return view('public.news-page', compact('news', 'categories', 'topNews'));
    }

    /**
     * Show news page
     *
     * @param int $id - id record
     * @return void
     */
    public function newsDetail(int $id)
    {
        $record = Blog::with('category')
            ->find($id);

        $categories = BlogCategory::orderBy('created_at', 'desc')
            ->limit(5)
            ->get();

        $topNews = Blog::orderByRaw('RAND()')
            ->limit(4)
            ->get();

        return view('public.news-details-page', compact('record', 'categories', 'topNews'));
    }

    /**
     * Show contact page
     *
     * @return view
     */
    public function contact()
    {
        return view('public.contact-page');
    }
}
