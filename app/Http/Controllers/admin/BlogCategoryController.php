<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    /**
     * Display a list of Blogs categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Admin Blog Categories';
        $pageActive = 'blogCategoryPage';
        $blogCategories = BlogCategory::paginate(10);

        return view('admin.blog-category.index', compact('pageActive', 'pageTitle', 'blogCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'pageTitle' => 'Admin create Blog Categories',
            'pageActive' => 'blogCategoryPage'
        ];

        return view('admin.blog-category.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:50',
        ]);

        $blogCategory = new BlogCategory();
        $blogCategory->name = $request->name;

        if ($blogCategory->save()) {
            return redirect()
                ->route('blog-category.index')
                ->with(['success' => __('blog-category.success_store')]);

        } else {
            return back()
                ->withErrors(['msg' => __('message.request_error')])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $pageTitle = 'Admin create Blog Categories';
        $pageActive = 'blogCategoryPage';
        $record = BlogCategory::findOrFail($id);

        return view('admin.blog-category.form', compact('pageTitle', 'pageActive', 'record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, int $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:50',
        ]);

        $blogCategory = BlogCategory::findOrFail($id);
        $blogCategory->name = $request->name;

        if ($blogCategory->save()) {
            return redirect()
                ->route('blog-category.index')
                ->with(['success' => __('blog-category.success_update')]);

        } else {
            return back()
                ->withErrors(['msg' => __('message.request_error')])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $blogCategory = BlogCategory::findOrFail($id);

        if ($blogCategory->delete()) {
            return redirect()
                ->route('blog-category.index')
                ->with(['success' => __('blog-category.success_delete')]);

        } else {
            return back()
                ->withErrors(['msg' => __('message.request_error')])
                ->withInput();
        }
    }
}
