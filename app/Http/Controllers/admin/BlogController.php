<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlog;
use App\Models\Blog;
use App\Models\BlogCategory;

use InterventionImage;
use File;

class BlogController extends Controller
{

    /**
     * Display a list of Blogs.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'pageTitle' => 'Admin Blogs',
            'pageActive' => 'blogPage',
            'blogs' => Blog::with('category')->paginate(10)
        ];

        return view('admin.blog.index', $data);
    }

    /**
     * Show the form for creating a new blog.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'pageTitle' => 'Admin create Blog',
            'pageActive' => 'blogPage',
            'blogCategories' => BlogCategory::all()
        ];

        return view('admin.blog.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreBlog $request)
    {
        $blog = new Blog();
        $blog->fill($request->all());

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = uniqid() . '.' . $image->getClientOriginalExtension();

            InterventionImage::make($image)
                ->save('images/blog/' . $imageName);

            $blog->image = $imageName;
        }

        if ($blog->save()) {
            return redirect()
                ->route('blog.index')
                ->with(['success' => __('blog.success_store')]);

        } else {
            return back()
                ->withErrors(['msg' => __('message.request_error')])
                ->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $data = [
            'pageTitle' => 'Admin create Blog',
            'pageActive' => 'blogPage',
            'blogCategories' => BlogCategory::all(),
            'record' => Blog::findOrFail($id)

        ];

        return view('admin.blog.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(StoreBlog $request, int $id)
    {
        $blog = Blog::findOrFail($id);
        $oldImage = $blog->image;

        $blog->fill($request->all());

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = uniqid() . '.' . $image->getClientOriginalExtension();

            InterventionImage::make($image)
                ->save('images/blog/' . $imageName);

            if (!is_null($oldImage)) {
                File::delete('images/blog/' . $oldImage);
            }

            $blog->image = $imageName;
        }

        if ($blog->save()) {
            return redirect()
                ->route('blog.index')
                ->with(['success' => __('blog.success_update')]);

        } else {
            return back()
                ->withErrors(['msg' => __('message.request_error')])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $blog = Blog::findOrFail($id);

        if ($blog->delete()) {
            return redirect()
                ->route('blog.index')
                ->with(['success' => __('blog.success_delete')]);

        } else {
            return back()
                ->withErrors(['msg' => __('message.request_error')])
                ->withInput();
        }
    }
}
