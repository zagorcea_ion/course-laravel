<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_category', 'title', 'description', 'content', 'image', 'created_at', 'updated_at'];

    /**
     * Get the category blog record associated with the blog.
     */
    public function category()
    {
        return $this->hasOne('App\Models\BlogCategory', 'id', 'id_category');
    }
}
