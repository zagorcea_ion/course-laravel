<?php

use App\Http\Controllers\admin\BlogCategoryController;
use App\Http\Controllers\admin\BlogController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [PageController::class, 'home'])->name('public.home');
Route::get('/contact', [PageController::class, 'contact'])->name('public.contact');
Route::get('/news', [PageController::class, 'news'])->name('public.news');
Route::get('news/category/{category}', [PageController::class, 'news'])->name('public.newsByCategory');

Route::get('/news/{news}', [PageController::class, 'newsDetail'])->name('public.newsDetail');

Route::prefix('admin-panel')->group(function () {
    //Blog
    Route::get('/blog/', [BlogController::class, 'index'])->name('blog.index');
    Route::get('/blog/create', [BlogController::class, 'create'])->name('blog.create');
    Route::post('/blog', [BlogController::class, 'store'])->name('blog.store');
    Route::get('/blog/{blog}/edit', [BlogController::class, 'edit'])->name('blog.edit');
    Route::put('/blog/{blog}', [BlogController::class, 'update'])->name('blog.update');
    Route::delete('/blog/{blog}', [BlogController::class, 'destroy'])->name('blog.destroy');

    //BlogCategory
    Route::resource('blog-category', BlogCategoryController::class);
});







Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
