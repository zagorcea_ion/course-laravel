<?php

return [
    'success_store' => 'Blog was successfully added',
    'success_update' => 'Blog was successfully updated',
    'success_delete' => 'Blog was successfully deleted',
];
