<?php

return [
    'success_store' => 'Blog category was successfully added',
    'success_update' => 'Blog category was successfully updated',
    'success_delete' => 'Blog category was successfully deleted',
];
