<div class="col-lg-4">
    <div class="blog_right_sidebar">

        <aside class="single_sidebar_widget post_category_widget">
            <h4 class="widget_title">Category</h4>
            <ul class="list cat-list">

                @foreach ($categories as $category)
                    <li>
                        <a href="{{ route('public.newsByCategory', ['category' => $category->id]) }}" class="d-flex">
                            <p>{{ $category->name }}</p>
                        </a>
                    </li>
                @endforeach

            </ul>
        </aside>

        <aside class="single_sidebar_widget popular_post_widget">
            <h3 class="widget_title">TOP Post</h3>

            @foreach ($topNews as $item)
                <div class="media post_item">
                    @if (!is_null($item->image))
                        <img src="{{ asset('images/blog/' . $item->image) }}" alt="{{ $item->title }}" width="100px">

                    @else
                        <img src="{{ asset('images/no-image.jpg') }}" alt="{{ $item->title }}" width="100px">
                    @endif

                    <div class="media-body">
                        <a href="{{ route('public.newsDetail', ['news' => $item->id]) }}">
                            <h3>{{ $item->title }}</h3>
                        </a>
                        <p>{{ date('F d, Y', strtotime($item->created_at)) }}</p>
                    </div>
                </div>
            @endforeach

        </aside>

    </div>
</div>
