@if ($paginator->hasPages())
    <nav class="blog-pagination justify-content-center d-flex">
        <ul class="pagination">

            <!-- Previous Page Link -->
            @if (!$paginator->onFirstPage())
                <li class="page-item">
                    <a href="{{ $paginator->previousPageUrl() }}" class="page-link" aria-label="Previous">
                        <i class="ti-angle-left"></i>
                    </a>
                </li>
            @endif

            @foreach ($elements as $element)

            <!-- Array Of Links -->
                @if (is_array($element))
                    @foreach ($element as $page => $url)

                    <!-- Active Link -->
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active">
                                <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                            </li>

                        @else
                            <li class="page-item">
                                <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif

            @endforeach

             <!-- Next Page Link -->
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a href="{{ $paginator->nextPageUrl() }}" class="page-link" aria-label="Next">
                        <i class="ti-angle-right"></i>
                    </a>
                </li>
            @endif
        </ul>
    </nav>

@endif
