@extends('layouts.public')

@section('main')
    <!--================Blog Area =================-->
    <section class="blog_area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="single-post">
                        <div class="feature-img">
                            @if (!is_null($record->image))
                                <img class="img-fluid" src="{{ asset('images/blog/' . $record->image) }}" alt="{{ $record->title }}">

                            @else
                                <img class="img-fluid" src="{{ asset('images/no-image.jpg') }}">
                            @endif

                        </div>
                        <div class="blog_details">
                            <h2>{{ $record->title }}</h2>
                            <ul class="blog-info-link mt-3 mb-4">
                                <li><a href="{{ route('public.newsByCategory', ['category' => $record->category->id]) }}"><i class="fa fa-bookmark"></i> {{ $record->category->name }}</a></li>
                                <li><a href="#"><i class="fa fa-calendar"></i> {{ date('d M', strtotime($record->created_at)) }}</a></li>
                            </ul>
                            {!! $record->content !!}
                        </div>
                    </div>
                </div>

                @include('public.components.sidebar')

            </div>
        </div>
    </section>
@endsection
