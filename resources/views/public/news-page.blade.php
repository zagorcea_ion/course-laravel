@extends('layouts.public')

@section('main')
    <!--================Blog Area =================-->
    <section class="blog_area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">

                        @foreach ($news as $item)
                            <article class="blog_item">
                                <div class="blog_item_img">
                                    @if (!is_null($item->image))
                                        <img class="card-img rounded-0" src="{{ asset('images/blog/' . $item->image) }}">

                                    @else
                                        <img class="card-img rounded-0" src="{{ asset('images/no-image.jpg') }}">
                                    @endif

                                    <a href="{{ route('public.newsDetail', ['news' => $item->id]) }}" class="blog_item_date">
                                        <h3>{{ date('d', strtotime($item->created_at)) }}</h3>
                                        <p>{{ date('M', strtotime($item->created_at)) }}</p>
                                    </a>
                                </div>

                                <div class="blog_details">
                                    <a class="d-inline-block" href="{{ route('public.newsDetail', ['news' => $item->id]) }}">
                                        <h2>{{ $item->title }}</h2>
                                    </a>
                                    <p>{{ $item->description }}</p>
                                    <ul class="blog-info-link">
                                        <li><a href="{{ route('public.newsByCategory', ['category' => $item->category->id]) }}"><i class="fa fa-bookmark"></i>{{ $item->category->name }}</a></li>
                                    </ul>
                                </div>
                            </article>
                        @endforeach

                        {{ $news->links('public.components.pagination') }}

                    </div>
                </div>

                @include('public.components.sidebar')

            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
@endsection
