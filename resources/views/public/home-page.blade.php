@extends('layouts.public')

@section('main')
    <main>
        <!-- Trending Area Start -->
        <div class="trending-area fix">
            <div class="container">
                <div class="trending-main">
                    <!-- Trending Tittle -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="trending-tittle">
                                <strong>Trending now</strong>
                                <!-- <p>Rem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                                <div class="trending-animated">
                                    <ul id="js-news" class="js-hidden">
                                        <li class="news-item">Bangladesh dolor sit amet, consectetur adipisicing elit.</li>
                                        <li class="news-item">Spondon IT sit amet, consectetur.......</li>
                                        <li class="news-item">Rem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($news as $item)

                            @if ($loop->first)
                                <div class="col-lg-8">
                                    <!-- Trending Top -->
                                    <div class="trending-top mb-30">
                                        <div class="trend-top-img">

                                            @if (!is_null($item->image))
                                                <img src="{{ asset('images/blog/' . $item->image) }}" alt="{{ $item->title }}">

                                            @else
                                                <img src="{{ asset('images/no-image.jpg') }}" alt="{{ $item->title }}">
                                            @endif

                                            <div class="trend-top-cap">
                                                <span>{{ $item->category->name }}</span>
                                                <h2><a href="{{ route('public.newsDetail', ['news' => $item->id]) }}">{{ $item->title }}</a></h2>
                                            </div>
                                        </div>
                                    </div>
                            @endif

                            @if ($loop->index == 1)
                                <!-- Trending Bottom -->
                                <div class="trending-bottom">
                                    <div class="row">
                            @endif

                                @if (in_array($loop->index, [1, 2, 3]))
                                <div class="col-lg-4">
                                    <div class="single-bottom mb-35">
                                        <div class="trend-bottom-img mb-30">
                                            @if (!is_null($item->image))
                                                <img src="{{ asset('images/blog/' . $item->image) }}" alt="{{ $item->title }}">

                                            @else
                                                <img src="{{ asset('images/no-image.jpg') }}" alt="{{ $item->title }}">
                                            @endif
                                        </div>
                                        <div class="trend-bottom-cap">
                                            <span class="color1">{{ $item->category->name }}</span>
                                            <h4><a href="{{ route('public.newsDetail', ['news' => $item->id]) }}">{{ $item->title }}</a></h4>
                                        </div>
                                    </div>
                                </div>
                                @endif


                            @if ($loop->index == 3)
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if ($loop->index == 4)
                                <!-- Riht content -->
                                <div class="col-lg-4">
                            @endif
                                @if (in_array($loop->index, [4, 5, 6, 7, 8]))
                                <div class="trand-right-single d-flex">
                                        <div class="trand-right-img">
                                            @if (!is_null($item->image))
                                                <img src="{{ asset('images/blog/' . $item->image) }}" alt="{{ $item->title }}" width="100px">

                                            @else
                                                <img src="{{ asset('images/no-image.jpg') }}" alt="{{ $item->title }}" width="100px">
                                            @endif
                                        </div>
                                        <div class="trand-right-cap">
                                            <span class="color1">{{ $item->category->name }}</span>
                                            <h4><a href="{{ route('public.newsDetail', ['news' => $item->id]) }}">{{ $item->title }}</a></h4>
                                        </div>
                                    </div>

                                @endif

                            @if ($loop->last)
                                </div>
                            @endif
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
        <!-- Trending Area End -->

        <!--  Recent Articles start -->
        <div class="recent-articles">
            <div class="container">
                <div class="recent-wrapper">
                    <!-- section Tittle -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-tittle mb-30">
                                <h3>Recent Articles</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="recent-active dot-style d-flex dot-style">
                                @foreach ($recentNews as $item)
                                    <div class="single-recent mb-100">
                                        <div class="what-img">
                                            @if (!is_null($item->image))
                                                <img src="{{ asset('images/blog/' . $item->image) }}" alt="{{ $item->title }}">

                                            @else
                                                <img src="{{ asset('images/no-image.jpg') }}" alt="{{ $item->title }}">
                                            @endif

                                        </div>
                                        <div class="what-cap">
                                            <span class="color1">{{ $item->category->name }}</span>
                                            <h4><a href="{{ route('public.newsDetail', ['news' => $item->id]) }}">{{ $item->title }}</a></h4>
                                        </div>
                                    </div>
                                @endforeach


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Recent Articles End -->
    </main>
@endsection


