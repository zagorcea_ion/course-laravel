@if ($paginator->hasPages())

    <div class="dataTables_paginate paging_simple_numbers">
        <ul class="pagination">


            <!-- Previous Page Link -->
            @if (!$paginator->onFirstPage())
                <li class="paginate_button page-item previous">
                    <a href="{{ $paginator->previousPageUrl() }}" class="page-link">{!!  __('pagination.previous') !!} </a>
                </li>
            @endif

            @foreach ($elements as $element)

                <!-- Array Of Links -->
                @if (is_array($element))
                    @foreach ($element as $page => $url)

                        <!-- Active Link -->
                        @if ($page == $paginator->currentPage())
                            <li class="paginate_button page-item active"><a href="{{ $url }}" class="page-link">{{ $page }}</a></li>
                        @else
                            <li class="paginate_button page-item ">
                                <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif

            @endforeach

            <!-- Next Page Link -->
            @if ($paginator->hasMorePages())
                <li class="paginate_button page-item next">
                    <a href="{{ $paginator->nextPageUrl() }}" class="page-link">{!! __('pagination.next') !!} </a>
                </li>
            @endif
        </ul>
    </div>

@endif
