@extends('layouts.admin')

@section('main')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Blog categories</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    @include('admin.components.error-message')

                    <!-- general form elements -->
                    <div class="card card-primary">

                        <div class="card-header">
                            <h3 class="card-title">{{ !empty($record) ? 'Edit' : 'Add' }} category</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="category-form" action="{{ !empty($record) ? route('blog-category.update', $record->id) : route('blog-category.store') }}" method="post">
                            {{ csrf_field() }}

                            @if (!empty($record))
                                {{ method_field('PUT') }}
                            @endif

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', !empty($record->name) ? $record->name : '') }}" placeholder="Name:">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </section>

@endsection
