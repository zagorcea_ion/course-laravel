@extends('layouts.admin')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('main')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Blogs</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('admin.components.success-message')
                    @include('admin.components.error-message')

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List of all blogs</h3>
                            <div class="card-tools">
                                <a href="{{ route('blog.create') }}" class="btn btn-success"><i class="fas fa-plus"></i> Add blog</a>
                                <a href="{{ route('blog-category.create') }}" class="btn btn-success"><i class="fas fa-plus"></i> Add category</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-bordered table-striped dataTable dtr-inline">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th width="200px">Category</th>
                                                <th width="150px">Image</th>
                                                <th width="150px">Date add</th>
                                                <th width="200px">Actions</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                                @foreach ($blogs as $blog)

                                                    <tr>
                                                        <td>{{ $blog->title }}</td>
                                                        <td>{{ $blog->category->name }}</td>
                                                        <td>

                                                            @if(!is_null($blog->image))
                                                                <img src="{{ asset('images/blog/' . $blog->image) }}" width="100px">
                                                            @else
                                                                <img src="{{ asset('images/no-image.jpg') }}" width="100px">
                                                            @endif

                                                        </td>
                                                        <td>{{ $blog->created_at }}</td>
                                                        <td>
                                                            <a href="{{ route('blog.edit', $blog->id) }}" class="btn btn-primary">
                                                                <i class="fas fa-edit"></i> Edit
                                                            </a>
                                                            <form action="{{ route('blog.destroy', $blog->id) }}" method="post" style="display: inline">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button class="btn btn-danger" type="submit"><i class="fas fa-trash-alt"></i> Delete</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Category</th>
                                                    <th>Image</th>
                                                    <th>Date add</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-5">
                                        <div class="dataTables_info">Showing 1 to 10 of 57 entries</div>
                                    </div>
                                    <div class="col-sm-12 col-md-7">
                                        {{ $blogs->links('admin.components.pagination') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
    </section>

@endsection
