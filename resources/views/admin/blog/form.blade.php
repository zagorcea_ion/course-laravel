@extends('layouts.admin')

@section('main')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Blog</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    @include('admin.components.error-message')

                <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{ !empty($record) ? 'Edit' : 'Add' }} blog</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ !empty($record) ? route('blog.update', $record->id) : route('blog.store') }}" method="post" enctype="multipart/form-data">
                            @csrf

                            @if (!empty($record))
                                @method('PUT')
                            @endif

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title', !empty($record->title) ? $record->title : '') }}" placeholder="Title:">
                                </div>

                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" id="description" name="description" rows="3" placeholder="Description:">{{ old('description', !empty($record->description) ? $record->description : '') }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="id-category">Category</label>
                                    <select class="form-control" id="id-category" name="id_category">
                                        <option selected="" disabled="">Select category</option>
                                        @foreach ($blogCategories as $category)
                                            <option value="{{ $category->id }}" {{ old('id_category', !empty($record->id_category) ? $record->id_category : '') == $category->id ? 'selected' : ''}}>
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="content">Content</label>
                                    <textarea class="form-control" id="content" name="content" rows="3" placeholder="Content ...">{{ old('content', !empty($record->content) ? $record->content : '') }}</textarea>
                                </div>


                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control validate[required]" id="image" name="image" placeholder="Image:">


                                    @if(!empty($record) && !is_null($record->image))
                                        <img src="{{ asset('images/blog/' . $record->image) }}" width="300px">

                                    @elseif (!empty($record) && is_null($record->image))
                                        <img src="{{ asset('images/no-image.jpg') }}" width="300px">
                                    @endif
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </section>

@endsection
